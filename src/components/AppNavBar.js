import React from "react";
import { useRef } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import "./AppNavBar.css";
import { BsSearch } from "react-icons/bs";
import { MdShoppingBasket } from "react-icons/md";
import { Link } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

function AppNavBar() {
  const { user } = useContext(UserContext);
  const navRef = useRef();

  const showNavbar = () => {
    navRef.current.classList.toggle("responsive_nav");
  };

  return (
    <header className="header Hresize">
      <Link as={Link} to="/">
        <img
          className="header__logo"
          src="https://i.imgur.com/oCxWsb8.png"
          alt="logo"
        />
      </Link>

      <div className="header__search">
        <input className="header__searchInput" type="text" />
        <BsSearch className="header__searchIcon" />
      </div>

      <nav className="header__nav" ref={navRef}>
        {user.id !== null ? (
          <div className="header__option">
            <span className="header__optionLineOne">Hello, Guest</span>
            <Link as={Link} to="/logout">
              <span className="header__optionLineTwo">Sign Out</span>
            </Link>
          </div>
        ) : (
          <>
            <div className="header__option">
              <span className="header__optionLineOne">Hello, Guest</span>
              <Link as={Link} to="/login">
                <span className="header__optionLineTwo">Sign In</span>
              </Link>
            </div>

            <div className="header__option">
              <span className="header__optionLineOne">New User?</span>
              <Link as={Link} to="/register">
                <span className="header__optionLineTwo">Register</span>
              </Link>
            </div>
          </>
        )}

        <div className="header__option returns">
          <span className="header__optionLineOne">Returns</span>

          <span className="header__optionLineTwo">& Orders</span>
        </div>
        <button className="nav-btn nav-close-btn" onClick={showNavbar}>
          <FaTimes />
        </button>
      </nav>

      <div className="header__optionBasket">
        <Link as={Link} to="/checkout">
          <MdShoppingBasket />
        </Link>
        <span className="header__optionLineTwo header__basketCount"></span>
      </div>

      <button className="nav-btn" onClick={showNavbar}>
        <FaBars />
      </button>
    </header>
  );
}

export default AppNavBar;
