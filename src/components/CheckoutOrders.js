import React from "react";
import { Col, Card, Button, Row } from "react-bootstrap";
import "./CheckoutOrders.css";
import UserContext from "../UserContext";
import { useContext } from "react";
import { AiFillStar } from "react-icons/ai";

export default function CheckoutOrders(orderProps) {
  const { user } = useContext(UserContext);
  const { breakpoint } = orderProps;
  const { productName, productPrice } = orderProps.orderProps;

  return (
    <Row className="myorders-view">
      <Col md={breakpoint} className="name-product-order">
        <p>
          <AiFillStar />
          {productName}
        </p>
        <span>${productPrice}</span>
        <hr />
      </Col>
    </Row>
  );
}
