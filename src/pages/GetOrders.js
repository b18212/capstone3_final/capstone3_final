import { useState, useEffect } from "react";
import CheckoutOrders from "../components/CheckoutOrders";
import { Row, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./GetOrders.css";

// hello
export default function GetOrders({ order }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(
      "https://floating-plains-89276.herokuapp.com/ customer/getSingleUser",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        data.forEach((item) => {
          setOrders(
            item.orders.map((product) => {
              console.log(product);
              return (
                <CheckoutOrders
                  key={product._id}
                  orderProps={product.productPrice}
                  breakpoint={2}
                />
              );
            })
          );
        });
      });
  }, [order]);

  return (
    <Card className="card_product-2 text-center">
      <Card.Body>
        <div>
          <h3>Your Orders</h3>
          <Card.Text className="price_order">{orders}</Card.Text>
        </div>
        <div>
          <Link className="btn btn-outline-primary m-1" to={"/products"}>
            {" "}
            Add more
          </Link>
        </div>
      </Card.Body>
    </Card>
  );
}
